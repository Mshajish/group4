package library.entities;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Book implements Serializable {
	
    private String title;
    private String author;
    private String callNo;
    private int id;
    private enum State{ AVAILABLE, ONLOAN, DAMAGED, RESERVED };
    private State state;

    public Book(String author, String title, String callNo, int id) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.id = id;
        this.state = State.AVAILABLE;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  Book: ").append(id).append("\n")
          .append("  Title: ").append(title).append("\n")
          .append("  Author: ").append(author).append("\n")
          .append("  CallNo: ").append(callNo).append("\n")
          .append("  State: ").append(state);
        return sb.toString();
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return state == State.AVAILABLE;
    }

    public boolean isOnLoan() {
        return state == State.ONLOAN;
    }

    public boolean isDamaged() {
        return state == State.DAMAGED;
    }

    public void borrow() {
        if (state.equals(State.AVAILABLE))
        {
            state = State.ONLOAN;
        }
        else{ 
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
        }
    }

    public void Return(boolean damaged) {  //"return" is a reserved keyword 
        if (state.equals(State.ONLOAN)) {
            if (damaged){ 
                state = State.DAMAGED;
            }
            else{ 
                state = State.AVAILABLE;
            }
        }
        else {
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
        }
    }

    public void repair() {
        if (state.equals(State.DAMAGED)) 
        {
            state = State.AVAILABLE;
        }
        else 
        {
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
        }
    }
}